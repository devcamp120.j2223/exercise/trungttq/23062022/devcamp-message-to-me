import { Component } from "react";
import likeimg from "../../../assets/images/like.png";

class OutputMessage extends Component {
    render() {
        return(
            <div>
            <div className="row">
                <div className="col-12">
                    <p>Thông điệp ở đây</p>
                </div>
            </div>

            <div className="row">
                <div className="col-12">
                    <img src={likeimg} width="50" alt="likeimage" />
                </div>
            </div>
        </div>
        )
        
    }
}
 export default OutputMessage;