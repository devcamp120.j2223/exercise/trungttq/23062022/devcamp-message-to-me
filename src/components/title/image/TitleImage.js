import { Component } from "react";
import backgroundImage from "../../../assets/images/background.jpg";

class TittleImage extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <img src={backgroundImage} width="500" alt="background" />
                </div>
            </div>
        )

    }
}
export default TittleImage;