import "bootstrap/dist/css/bootstrap.min.css";
import backgroundimage from "./assets/images/background.jpg";
import like from "./assets/images/like.png";
import TitleComponent from "./components/title/TitleComponent";
import ContentComponent from "./components/content/ContentComponent";
function App() {
  return (
    <div className="container text-center">
      <TitleComponent/>
      <ContentComponent/>
    </div>
  );
}

export default App;
